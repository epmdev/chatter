function initializeNewSummernote(id) {
    return $(id).summernote({
        height: 300,                 // set editor height
        minHeight: 200,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style']],
            ['table', ['table']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['fontname', ['fontname']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'help']]
        ]
    });
}

$('document').ready(function() {
    var summernote = initializeNewSummernote(document.getElementById("summernote"));
    var summernoteInDiscussionView = initializeNewSummernote(document.getElementById('summernote_in_discussion_view'));

    summernote.on('init', function() {
        document.getElementById('new_discussion_loader').style.display = "none";
        document.getElementById('chatter_form_editor').style.display = "block";

        // check if user is in discussion view
        if ($('#new_discussion_loader_in_discussion_view').length > 0) {
            document.getElementById('new_discussion_loader_in_discussion_view').style.display = "none";
            document.getElementById('chatter_form_editor_in_discussion_view').style.display = "block";
        }
    });


    $('.editor-toolbar .fa-columns').click(function(){
        if(!$('body').hasClass('summernote')){
            $('body').addClass('summernote');
        }
    });

    $('.editor-toolbar .fa-arrows-alt').click(function(){
        if($('body').hasClass('summernote')){
            $('body').removeClass('summernote');
        } else {
            $('body').addClass('summernote');
        }
    });

    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            inputVal = input.val(),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = inputVal.replace(/\\/g, '/').replace(/.*\//, '');
        var log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
            $('#documents_info').html(log);
        }
    });
});