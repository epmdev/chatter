<?php

namespace EpmDev\Chatter\Controllers;

use EpmDev\Chatter\Models\Discussion;
use EpmDev\Chatter\Models\Models;
use EpmDev\Chatter\Models\Post;
use EpmDev\Chatter\Models\File;
use EpmDev\Chatter\Models\SuspendedUsers;
use App\User;
use EpmDev\Chatter\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Http\Controllers\Controller;

class ChatterManagementController extends Controller
{
    protected $members;

    public function __construct()
    {

    }

    public function index()
    {
        $categories = Category::all();
        $suspendedUsers = SuspendedUsers::where('suspended', '=', 1)->get();
        $users = User::all();
        $maxOrder = $categories->max('order') + 1;

        return view('chatter::management')->withPageTitle('Forum Management')
            ->withCategories($categories)
            ->withSuspendedUsers($suspendedUsers)
            ->withMaxOrder($maxOrder)
            ->withUsers($users);
    }

    public function addCategory(Request $request)
    {
        if (auth()->user()->hasRole(config('chatter.management_roles'))) {
            if(!empty($request->name)) {
                if($request->add_parent == "on") {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|min:4|unique:chatter_categories',
                        'color' => 'required',
                        'parent_category' => 'required|numeric',
                        'category_order'  => 'required|numeric',
                    ]);
                    $category_data = [
                        'name' => $request->name,
                        'color' => $request->color,
                        'locked' => 0,
                        'order' => $request->category_order,
                        'parent_id' => $request->parent_category
                    ];
                } else {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|min:4|unique:chatter_categories',
                        'color' => 'required',
                        'category_order'  => 'required|numeric',
                    ]);
                    $category_data = [
                        'name' => $request->name,
                        'color' => $request->color,
                        'locked' => 0,
                        'order' => $request->category_order,
                    ];
                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                }

                try {
                    $category_data['slug'] = str_slug($category_data['name'], '_');

                    $category_exists = Models::category()->where('slug', '=', $category_data['slug'])->first();

                    if (!empty($category_exists->id)) {
                        return redirect()->back()->withErrors(array('message' => 'Category name already exists. Please chose another name'));
                    }

                    $category = new Category($category_data);

                    if ($category->save()) {
                        return redirect()->back();
                    }
                } catch (\Exception $ex) {
                    return redirect()->back()->withErrors($ex->getMessage());
                }
            }
            return redirect()->back()->withErrors(array('message' => 'There was a problem with your form submission, please try again.'));
        }
        return redirect()->back()->withErrors(array('message' => 'You do not have the permission to do this.'));
    }

    public function lockCategory(Request $request)
    {
        if (auth()->user()->hasRole(config('chatter.management_roles'))) {
            if(!empty($request->lock_category_id) && is_numeric($request->lock_category_id)) {
                try {
                    $category = Category::find($request->lock_category_id);

                    if(!empty($category->id)) {
                        $category->locked = ($request->lock_category_status == 1) ? 0 : 1;
                        if($category->save()) {
                            return redirect()->back();
                        }
                    }
                } catch(\Exception $ex) {
                    return redirect()->back()->withErrors($ex->getMessage());
                }
            }
            return redirect()->back()->withErrors(array('message' => 'There was an error with the form submission, please try again.'));
        }
        return redirect()->back()->withErrors(array('message' => 'You do not have the permission to do this.'));
    }

    public function stickyDiscussion(Request $request, $id)
    {
        if (auth()->user()->hasRole(config('chatter.management_roles'))) {
            try {
                $discussion = Discussion::find($id);
                $discussion->sticky = ($discussion->sticky) ? 0 : 1;
                if ($discussion->save()) {
                    return redirect()->back();
                }
            } catch (\Exception $ex) {
                return redirect()->back()->withErrors($ex->getMessage());
            }

        }
        return redirect()->back()->withErrors(array('message' => 'You do not have the permission to do this.'));
    }


    /**
     * @param Request $request
     */
    public function updateCategory(Request $request)
    {
        if (auth()->user()->hasRole(config('chatter.management_roles'))) {
            if(!empty($request->id) && is_numeric($request->id)) {
                try {
                    $category = Category::find($request->id);
                    $slug = str_slug($request->name, '_');
                    $name_exists = Models::category()->where('slug', '=', $slug)->first();

                    if (!empty($name_exists->id) && $category->id != $name_exists->id) {
                        return redirect()->back()->withErrors(array('message' => 'Category name already exists. Please chose another name'));
                    }

                    if (!empty($request->parent_category)) {
                        if($request->parent_category == 'no_parent') {
                            $category->parent_id = null;
                        }
                        $category->parent_id = $request->parent_category;
                    }
                    $category->order = $request->category_order;
                    $category->name = $request->name;
                    if (!empty($request->color)) {
                        $category->color = $request->color;
                    }
                    $category->slug = $slug;

                    if($category->save()) {
                        return redirect()->back();
                    }
                } catch (Exception $ex) {
                    return redirect()->back()->withErrors($ex->getMessage());
                }
            }
            return redirect()->back()->withErrors(array('message' => 'There was an error with the form submission, please try again.'));
        }
        return redirect()->back()->withErrors(array('message' => 'You do not have the permission to do this.'));
    }

    /**
     * @param Request $request
     */
    public function deleteCategory(Request $request)
    {
        if (auth()->user()->hasRole(config('chatter.management_roles'))) {
            if (!empty($request->category_id) && is_numeric($request->category_id)) {
                DB::beginTransaction();
                try {
                    $category = Category::find($request->category_id);
                    $discussions = $category->discussions;
                    $allPosts= [];
                    $allFiles = [];

                    foreach($discussions as $discussion) {
                        array_push($allPosts, $discussion->posts);
                        Discussion::destroy($discussion->id);
                    }

                    foreach ($allPosts as $posts) {
                        foreach($posts as $post) {
                            array_push($allFiles, $post->files);
                            Post::destroy($post->id);
                        }
                    }

                    foreach($allFiles as $files) {
                        foreach($files as $file) {
                            Storage::disk('local')->delete($file->filename);
                            File::destroy($file->id);
                        }
                    }
                    Category::destroy($category->id);
                    DB::commit();
                    return redirect()->back();

                } catch (\Exception $ex) {
                    DB::rollBack();
                    return redirect()->back()->withErrors($ex->getMessage());
                }
            }
            return redirect()->back()->withErrors(array('message' => 'There was an error with the form submission, please try again.'));
        }
        return redirect()->back()->withErrors(array('message' => 'You do not have the permission to do this.'));
    }

    /**
     * @param Request $request
     */
    public function suspendUser(Request $request)
    {
        if (auth()->user()->hasRole(config('chatter.management_roles'))) {
            if(!empty($request->suspend_user_id) && is_numeric($request->suspend_user_id)) {
                try {
                    $suspended = SuspendedUsers::where('user_id', '=', $request->suspend_user_id)->first();

                    if (!empty($suspended->user_id)) {
                        $suspended->suspended = 1;
                        if ($suspended->save()) {
                            return redirect()->back();
                        }
                    } else {
                        $data = [
                            'user_id' => $request->suspend_user_id,
                            'suspended' => 1
                        ];

                        $suspended = new SuspendedUsers($data);
                        if ($suspended->save()) {
                            return redirect()->back();
                        }
                    }
                } catch(\Exception $ex) {
                    return redirect()->back()->withErrors($ex->getMessage());
                }
            }
            return redirect()->back()->withErrors(array('message' => 'There was an error with the form submission, please try again.'));
        }
        return redirect()->back()->withErrors(array('message' => 'You do not have the permission to do this.'));
    }

    /**
     * @param User $user
     */
    public function restoreUser(Request $request)
    {
        if (auth()->user()->hasRole(config('chatter.management_roles'))) {
            if(!empty($request->restore_user_id) && is_numeric($request->restore_user_id)) {
                try {
                    $suspended = SuspendedUsers::where('user_id', '=', $request->restore_user_id)->first();

                    if (!empty($suspended->user_id)) {
                        $suspended->suspended = 0;
                        if ($suspended->save()) {
                            return redirect()->back();
                        }
                    }
                    return redirect()->back()->withErrors(array('message' => 'There was an error with the form submission, please try again.'));
                } catch(\Exception $ex) {
                    return redirect()->back()->withErrors($ex->getMessage());
                }
            }
            return redirect()->back()->withErrors(array('message' => 'There was an error with the form submission, please try again.'));
        }
        return redirect()->back()->withErrors(array('message' => 'You do not have the permission to do this.'));
    }
}
