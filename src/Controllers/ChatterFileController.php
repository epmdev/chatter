<?php


namespace EpmDev\Chatter\Controllers;

use Auth;
use Carbon\Carbon;
use Event;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Validator;


class ChatterFileController extends Controller
{
    public function add(Request $request)
    {
        // move to repository and custom request
        $user = auth()->user();

        $file = $request->file('filefield');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename().'.'.$extension, File::get($file));
        $document = new Document();
        $document->mime = $file->getClientMimeType();
        $document->original_filename = $file->getClientOriginalName();
        $document->filename = $file->getFilename().'.'.$extension;
        $document->document_type = $request->input('document_type');

        if ($document->save()) {
           return Response::json(['success' => true, 'filename' => $document->filename ]);
        }
    }

    public function download($filename)
    {

        // must be admin or owner to download documents
        if (auth()->user()->hasRole(['admin', 'owner', 'member'])) {
            $file = storage_path('app') . '/' . $filename;

            return response()->download($file);
        }

        return redirect()->back()->withFlashDanger('You are not authorized to download this document');
    }

    public function destroyByAjax($id)
    {
        $document = Document::find($id);
        if ((auth()->user()->hasRole(['member', 'applicant']) && auth()->user()->member->id == $document->member_id) || auth()->user()->hasRole(['admin','owner'] )) {
            if (Storage::disk('local')->delete($document->filename) && $document->delete()) {
                return \Response::json(array(
                    'succeed' => true
                ));
            }
        }
        return \Response::json(array(
            'succeed' => false
        ));
    }
}