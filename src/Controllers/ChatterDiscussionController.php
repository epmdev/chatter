<?php

namespace EpmDev\Chatter\Controllers;

use App\User;
use Auth;
use Carbon\Carbon;
use EpmDev\Chatter\Events\ChatterAfterNewDiscussion;
use EpmDev\Chatter\Events\ChatterBeforeNewDiscussion;
use EpmDev\Chatter\Models\Category;
use EpmDev\Chatter\Models\Discussion;
use EpmDev\Chatter\Models\File;
use EpmDev\Chatter\Models\Post;
use EpmDev\Chatter\UserNameSearchAspect;
use Illuminate\Support\Facades\File as LaravelFile;
use EpmDev\Chatter\Models\Models;
use Event;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Support\Facades\Storage;
use Spatie\Searchable\ModelSearchAspect;
use Spatie\Searchable\Search;
use Validator;
use Yajra\DataTables\Utilities\Helper;
use EpmDev\Chatter\Helpers\ChatterHelper as ChatterHelper;

class ChatterDiscussionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*$total = 10;
        $offset = 0;
        if ($request->total) {
            $total = $request->total;
        }
        if ($request->offset) {
            $offset = $request->offset;
        }
        $discussions = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->orderBy('created_at', 'ASC')->take($total)->offset($offset)->get();*/

        // Return an empty array to avoid exposing user data to the public.
        // This index function is not being used anywhere.
        return response()->json([]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Models::category()->all();

        return view('chatter::discussion.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['body_content' => strip_tags($request->body)]);
        $documents = [];
        if ($request->hasFile('documents')) {
            $documents = $request->file('documents');
        }

        $validator = Validator::make($request->all(), [
            'title'               => 'required|min:4|max:255',
            'body_content'        => 'required|min:10',
            'chatter_category_id' => 'required',
         ],[
			'title.required' =>  trans('chatter::alert.danger.reason.title_required'),
			'title.min'     => [
				'string'  => trans('chatter::alert.danger.reason.title_min'),
			],
			'title.max' => [
				'string'  => trans('chatter::alert.danger.reason.title_max'),
			],
			'body_content.required' => trans('chatter::alert.danger.reason.content_required'),
			'body_content.min' => trans('chatter::alert.danger.reason.content_min'),
			'chatter_category_id.required' => trans('chatter::alert.danger.reason.category_required'),
		]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $category = Category::find($request->chatter_category_id);


        if ($category->locked) {
            return redirect()->back()->withErrors(array('message' => 'This category is locked and new discussions cannot be created. Please choose another category.'));
        }
        

        Event::{config('chatter.laravel_event_firing')}(new ChatterBeforeNewDiscussion($request, $validator));

        if (function_exists('chatter_before_new_discussion')) {
            chatter_before_new_discussion($request, $validator);
        }

        $user_id = Auth::user()->id;

        if (config('chatter.security.limit_time_between_posts')) {
            if ($this->notEnoughTimeBetweenDiscussion()) {
                $minutes = trans_choice('chatter::messages.words.minutes', config('chatter.security.time_between_posts'));
                $chatter_alert = [
                    'chatter_alert_type' => 'danger',
                    'chatter_alert'      => trans('chatter::alert.danger.reason.prevent_spam', [
                                                'minutes' => $minutes,
                                            ]),
                    ];

                return redirect('/'.config('chatter.routes.home'))->with($chatter_alert)->withInput();
            }
        }

        // *** Let's gaurantee that we always have a generic slug *** //
        $slug = str_slug($request->title, '-');

        $discussion_exists = Models::discussion()->where('slug', '=', $slug)->withTrashed()->first();
        $incrementer = 1;
        $new_slug = $slug;
        while (isset($discussion_exists->id)) {
            $new_slug = $slug.'-'.$incrementer;
            $discussion_exists = Models::discussion()->where('slug', '=', $new_slug)->withTrashed()->first();
            $incrementer += 1;
        }

        if ($slug != $new_slug) {
            $slug = $new_slug;
        }

        $new_discussion = [
            'title'               => $request->title,
            'chatter_category_id' => $request->chatter_category_id,
            'user_id'             => $user_id,
            'slug'                => $slug,
            'color'               => $request->color,
            ];

        $category = Models::category()->find($request->chatter_category_id);
        if (!isset($category->slug)) {
            $category = Models::category()->first();
        }

        $discussion = Models::discussion()->create($new_discussion);

        $new_post = [
            'chatter_discussion_id' => $discussion->id,
            'user_id'               => $user_id,
            'body'                  => $request->body,
            ];

        if (config('chatter.editor') == 'simplemde'):
           $new_post['markdown'] = 1;
        endif;

        // add the user to automatically be notified when new posts are submitted
        $discussion->users()->attach($user_id);

        $post = Models::post()->create($new_post);

        if ($post->id) {

            if (!empty($documents)) {
                foreach ($documents as $doc) {
                    $extension = $doc->getClientOriginalExtension();
                    Storage::disk('local')->put($doc->getFilename().'.'.$extension, LaravelFile::get($doc));
                    $document = new File();
                    $document->user_id = $user_id;
                    $document->post_id = $post->id;
                    $document->mime = $doc->getClientMimeType();
                    $document->original_filename = $doc->getClientOriginalName();
                    $document->filename = $doc->getFilename().'.'.$extension;
                    $document->save();
                }
            }

            Event::{config('chatter.laravel_event_firing')}(new ChatterAfterNewDiscussion($request, $discussion, $post));

            if (function_exists('chatter_after_new_discussion')) {
                chatter_after_new_discussion($request);
            }

            $chatter_alert = [
                'chatter_alert_type' => 'success',
                'chatter_alert'      => trans('chatter::alert.success.reason.created_discussion'),
                ];

            return redirect('/'.config('chatter.routes.home').'/'.config('chatter.routes.discussion').'/'.$category->slug.'/'.$slug)->with($chatter_alert);
        } else {
            $chatter_alert = [
                'chatter_alert_type' => 'danger',
                'chatter_alert'      => trans('chatter::alert.danger.reason.create_discussion'),
            ];

            return redirect('/'.config('chatter.routes.home').'/'.config('chatter.routes.discussion').'/'.$category->slug.'/'.$slug)->with($chatter_alert);
        }
    }

    private function notEnoughTimeBetweenDiscussion()
    {
        $user = Auth::user();

        $past = Carbon::now()->subMinutes(config('chatter.security.time_between_posts'));

        $last_discussion = Models::discussion()->where('user_id', '=', $user->id)->where('created_at', '>=', $past)->first();

        if (isset($last_discussion)) {
            return true;
        }

        return false;
    }

    public function search(Request $request)
    {
        $searchResults = (new Search())
            ->registerAspect(UserNameSearchAspect::class)
            ->registerModel(Discussion::class, 'title', 'updated_at', 'created_at')
            ->registerModel(Post::class, 'body', 'updated_at', 'created_at')
            ->search($request->search);

        $results = [];
        if($searchResults->count() > 0) {
            foreach ($searchResults as $result) {
                $descriptor = '';
                $divider = '';
                if($result->type == 'chatter_post') {
                    $descriptor = 'Post';
                    $divider = ' - ';
                } elseif($result->type == 'chatter_discussion') {
                    $descriptor = 'Discussion';
                    $divider = ' - ';
                }
                array_push($results, '<div class="search-results-row dropdown-item" onclick="location.href=\''.$result->url.'\'" style="cursor:pointer;">'. $descriptor. $divider .$result->title.'</div>');
            }
        } else {
            $results = [0 => '<div class="search-results-row dropdown-item" >No results found</div>'];
        }
        $data = ['success' => true, 'data' => $results];
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($category, $slug = null)
    {
        if (!isset($category) || !isset($slug)) {
            return redirect(config('chatter.routes.home'));
        }

        $discussion = Models::discussion()->where('slug', '=', $slug)->first();
        if (is_null($discussion)) {
            abort(404);
        }

        $discussion_category = Models::category()->find($discussion->chatter_category_id);
        if ($category != $discussion_category->slug) {
            return redirect(config('chatter.routes.home').'/'.config('chatter.routes.discussion').'/'.$discussion_category->slug.'/'.$discussion->slug);
        }
        $posts = Models::post()->with('user')->withTrashed()->where('chatter_discussion_id', '=', $discussion->id)->orderBy(config('chatter.order_by.posts.order'), config('chatter.order_by.posts.by'))->paginate(10);
        $discussionLocked = ChatterHelper::discussionLocked($category, $discussion->id);

        $chatter_editor = config('chatter.editor');

        if ($chatter_editor == 'simplemde') {
            // Dynamically register markdown service provider
            \App::register('GrahamCampbell\Markdown\MarkdownServiceProvider');
        }

        $discussion->increment('views');
        $userCanPost = ChatterHelper::userCanPost(auth()->user());
        
        return view('chatter::discussion', compact('discussion', 'discussionLocked', 'userCanPost','posts', 'chatter_editor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function sanitizeContent($content)
    {
        libxml_use_internal_errors(true);
        // create a new DomDocument object
        $doc = new \DOMDocument();

        // load the HTML into the DomDocument object (this would be your source HTML)
        $doc->loadHTML($content);

        $this->removeElementsByTagName('script', $doc);
        $this->removeElementsByTagName('style', $doc);
        $this->removeElementsByTagName('link', $doc);

        // output cleaned html
        return $doc->saveHtml();
    }

    private function removeElementsByTagName($tagName, $document)
    {
        $nodeList = $document->getElementsByTagName($tagName);
        for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0;) {
            $node = $nodeList->item($nodeIdx);
            $node->parentNode->removeChild($node);
        }
    }

    public function toggleEmailNotification($category, $slug = null)
    {
        if (!isset($category) || !isset($slug)) {
            return redirect(config('chatter.routes.home'));
        }

        $discussion = Models::discussion()->where('slug', '=', $slug)->first();

        $user_id = Auth::user()->id;

        // if it already exists, remove it
        if ($discussion->users->contains($user_id)) {
            $discussion->users()->detach($user_id);

            return response()->json(0);
        } else { // otherwise add it
             $discussion->users()->attach($user_id);

            return response()->json(1);
        }
    }
}
