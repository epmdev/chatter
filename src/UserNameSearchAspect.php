<?php


namespace EpmDev\Chatter;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\Searchable\SearchAspect;
use App\User;
use Spatie\Searchable\SearchResult;
use Spatie\Searchable\SearchResultCollection;


class UserNameSearchAspect extends SearchAspect
{
    /**
     * If your user table does not contain your primary user data like names etc...
     * This search aspect should be overridden to meet your applications needs
     * @param string $term
     * @return Collection
     */
    public function getResults(string $term): Collection
    {
        $searchTerm = '%'.$term.'%';
        $results = DB::select(
            'select `users`.`id` as `user_id`, `chatter_post`.`id` as `post_id`, 
                    `chatter_categories`.`slug` as `cat_slug`, `chatter_discussion`.`slug` as `disc_slug`,
                    `chatter_discussion`.`title` as `discussion`
                    from `users` 
                    inner join `chatter_post` on `chatter_post`.`user_id` = `users`.`id` 
                    inner join `chatter_discussion` on `chatter_discussion`.`id` = `chatter_post`.`chatter_discussion_id` 
                    inner join `chatter_categories` on `chatter_categories`.`id` = `chatter_discussion`.`chatter_category_id` 
                    left join `members` on `members`.`user_id` = `users`.`id` 
                    left join `user_details` on `user_details`.`user_id` = `users`.`id` 
                    where (`members`.`first_name` LIKE ? OR `members`.`last_name` LIKE ? OR `user_details`.`name` LIKE ?
                        OR CONCAT(`members`.`first_name`, " ", `members`.`last_name`) LIKE ? )', [$searchTerm, $searchTerm, $searchTerm, $searchTerm]);
        $collection = new Collection();
        if(count($results) > 0) {
            foreach ($results as $result) {
                $user = User::find($result->user_id);
                $user->url = config('chatter.routes.home') .'/'.config('chatter.routes.discussion').'/'.$result->cat_slug.'/'.$result->disc_slug.'/#post-'.$result->post_id;
                $user->discussion_title = $result->discussion;
                $collection->add($user);
            }
        }
        return $collection;
    }
}