<?php

namespace EpmDev\Chatter\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    protected $table = 'chatter_files';
    public $timestamps = true;
    protected $fillable = ['user_id', 'post_id','filename', 'mime', 'original_filename'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(config('chatter.user.namespace'));
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
