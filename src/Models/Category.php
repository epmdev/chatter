<?php

namespace EpmDev\Chatter\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'chatter_categories';
    protected $fillable = [
        'parent_id', 'order', 'locked', 'name', 'color', 'slug'
    ];
    public $timestamps = true;
    public $with = 'children';
    protected $dates = ['deleted_at'];

    public function discussions()
    {
        return $this->hasMany(Models::className(Discussion::class),'chatter_category_id');
    }

    public function parent()
    {
        return $this->belongsTo(Models::classname(self::class), 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Models::classname(self::class), 'parent_id')->orderBy('order', 'asc');
    }

}
