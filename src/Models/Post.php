<?php

namespace EpmDev\Chatter\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Searchable\SearchResult;
use Spatie\Searchable\Searchable;

class Post extends Model implements Searchable
{
    use SoftDeletes;
    
    protected $table = 'chatter_post';
    public $timestamps = true;
    protected $fillable = ['chatter_discussion_id', 'user_id', 'body', 'markdown'];
    protected $dates = ['deleted_at'];
    protected $touches = ['discussion'];

    public function discussion()
    {
        return $this->belongsTo(Models::className(Discussion::class), 'chatter_discussion_id');
    }

    public function user()
    {
        return $this->belongsTo(config('chatter.user.namespace'));
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function getSearchResult(): SearchResult
    {
        $discussion = Discussion::where('id', '=', $this->chatter_discussion_id)->first();
        $discussion_category = Category::where('id', '=', $discussion->chatter_category_id)->first();
        $url = config('chatter.routes.home').'/'.config('chatter.routes.discussion').'/'.$discussion_category->slug.'/'.$discussion->slug.'/#post-'.$this->id;

        return new \Spatie\Searchable\SearchResult($this, str_limit(strip_tags($this->body), 30), $url);
    }
}
