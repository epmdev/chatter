<?php

namespace EpmDev\Chatter\Models;

use Illuminate\Database\Eloquent\Model;

class SuspendedUsers extends Model
{
    protected $table = 'chatter_posting_permission';
    protected $fillable = ['user_id', 'suspended'];

    public function user()
    {
        return $this->belongsTo(config('chatter.user.namespace'));
    }
}
