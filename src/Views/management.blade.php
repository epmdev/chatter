@extends(Config::get('chatter.master_file_extend'))
@section(Config::get('chatter.yields.head'))
    <link href="{{ url('/vendor/epmdev/chatter/assets/vendor/spectrum/spectrum.css') }}" rel="stylesheet">
    <link href="{{ url('/vendor/epmdev/chatter/assets/css/chatter.css') }}" rel="stylesheet">
    <style type="text/css">
        .sp-preview-inner, .sp-alpha-inner, .sp-thumb-inner{
            border-radius: 30px;
            height: 26px;
            width: 26px;
        }

        .sp-preview{
            width:28px;
            height:28px;
            border-color:#7089A9;
            border-radius:30px;
        }

        .sp-replacer{
            border-radius:30px;
            border: solid 2px #f1f5f9;
            background: none;
        }

        .sp-dd{
            margin-right:4px;
            line-height:0px;
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -o-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
            color: #7089A9;
            top: 3px;
            padding-left: 5px;
        }
        .category-modal {
            padding-bottom: 10px;
        }
    </style>
@stop
@section('content')
        @if (count($errors) > 0)
            <div class="col-md-12">
                <div class="alert alert-danger"><ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul></div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-6">
                <h4>Categories:</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <button type="button" class="btn btn-link pull-right" data-toggle="modal"
                                data-target="#categoryAdd">
                            Add
                        </button>
                        <div class='row'>
                            <div class='col-md-12'>
                                <table class="table table-striped" id="members" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Colour</th>
                                        <th>Discussions</th>
                                        <th>Parent</th>
                                        <th>Order</th>
                                        <th>Is Locked</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($categories as $category)
                                        <?php
                                        $parent = 'Top Level Category';
                                        if (!empty($category->parent_id)) {
                                            $parentCategory = \EpmDev\Chatter\Models\Category::find($category->parent_id);
                                            if (!empty($parentCategory->name)) {
                                                $parent = $parentCategory->name;
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td>{{$category->name}}</td>
                                            <td><i class="chatter-square" style="color:{{ $category->color }}"></i>
                                            </td>
                                            <td>{{$category->discussions->count() }}</td>
                                            <td>{{ $parent }} </td>
                                            <td>{{$category->order}}</td>
                                            <td>
                                                <a class="text-danger lock-category"
                                                   href="javascript:void(0);"
                                                   aria-label="{{($category->locked) ? 'Unlock' : 'Lock'}} {{ $category->name ?? 'Category' }}"
                                                   data-toggle="tooltip"
                                                   data-title="{{($category->locked) ? 'Unlock' : 'Lock'}} this category"
                                                   data-category-id="{{$category->id}}">
                                                    <i class="{{($category->locked) ? 'chatter-lock-close' : 'chatter-lock-open'}} {{($category->locked) ? 'text-danger' : 'text-success'}}"></i>
                                                </a> {{($category->locked) ? 'Locked' : 'Open'}}</td>
                                            <td>
                                                <a class="text-info edit-category" href="javascript:void(0);"
                                                   aria-label="Edit {{ $category->name ?? 'Category' }}"
                                                   data-toggle="tooltip" data-title="Edit this category"
                                                   data-category-id="{{$category->id}}"
                                                   data-category-name="{{$category->name}}"
                                                   data-category-order="{{$category->order}}" data-category-parent="{{!empty($category->parent_id) ? $category->parent_id : 0}}"
                                                   data-category-status="{{$category->locked}}"
                                                   data-category-color="{{$category->color}}"><i
                                                            class="chatter-edit"></i></a> |
                                                <a class="text-danger delete-category" href="javascript:void(0);"
                                                   aria-label="Delete {{ $category->name ?? 'Category' }}"
                                                   data-toggle="tooltip"
                                                   data-title="Delete @if($category->name)&ldquo;{{ $category->name }}&rdquo;@else this category @endif"
                                                   data-category-id="{{ $category->id }}"
                                                   data-category-name="{{$category->name}}"><i
                                                            class="chatter-delete"></i></a>
                                                <form action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.management') }}/{{ Config::get('chatter.routes.deleteCategory') }}"
                                                      method="post" style="display: none;"
                                                      id="category-delete-{{ $category->id }}">
                                                    @csrf
                                                    <input type="hidden" name="category_id"
                                                           value="{{$category->id}}">
                                                    <input type="submit" value="submit">
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>No Categories Found</tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h4>Suspended Users:</h4>
                <div class="panel panel-default">
                    <div class="panel-body">

                        <button id="licence-edit" type="button" class="btn btn-link pull-right" data-toggle="modal"
                                data-target="#suspendPosting">
                            Add
                        </button>

                        <div class='row'>
                            <div class='col-md-12'>
                                <table class="table table-striped" id="members" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Suspended</th>
                                        <th>Updated On</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($suspendedUsers as $suspended)
                                        <?php
                                        $suspendedIcon = '';
                                        $suspendedColor = 'text-success';
                                        if ($suspended->suspended) {
                                            $suspendedIcon = 'chatter-check-two';
                                            $suspendedColor = 'text-danger';
                                        }
                                        $statusChange = ($suspended->suspended) ? '<a   href="javascript:void(0);"
                                                                                    class="restore_user"
                                                                                    data-user-id="' . $suspended->user_id . '">Restore</a>' :
                                            '<a href="javascript:void(0);"
                                                                                    class="suspend_user"
                                                                                    data-user-id="' . $suspended->user_id . '">Suspend</a>';
                                        ?>
                                        <tr>
                                            <td>{{ $suspended->user->name }}</td>
                                            <td class="{{ $suspendedColor }}"><i
                                                        class="{{ $suspendedIcon }}"></i> {{ (empty($suspendedIcon) ? 'No' : '') }}
                                            </td>
                                            <td>{{ $suspended->updated_at->format('Y-m-d') }}</td>
                                            <td><?php echo $statusChange; ?></td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>No Suspended User history</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal modal-edit" id="categoryAdd"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title"  id="add-update-modal">Add Forum Category</h4>
                    </div>
                    <div class="modal-body">
                        <form id="addCategoryForm"
                              action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.management') }}/{{ Config::get('chatter.routes.addCategory') }}"
                              class="form-inline"
                              method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-sm-12 category-modal">
                                <label for="name" class="control-label col-sm-4">Category Name:</label>
                                <input id="category_name" name="name" type="text"
                                       class="form-control col-sm-8" size="40" required>
                            </div>
                            <div class="form-group col-sm-12 category-modal">
                                <label for="add_parent" class="control-label col-sm-4">Add Parent Category:</label>
                                <input type="checkbox" id="add_parent" name="add_parent">
                            </div>
                            <div class="form-group col-sm-12 category-modal hidden" id="parent_group">
                                <label for="parent_category" class="control-label col-sm-4">Parent Category:</label>
                                <select class="form-control" id="parent_category" name="parent_category">
                                    <option data-parent-id="" value="" disabled>Please select a category...</option>
                                    <option data-parent-id="" value="no_parent">No Parent Category</option>
                                    @forelse($categories as $category)
                                    <option data-parent-id="{{$category->parent_id}}" value="{{$category->id}}">{{$category->name}}</option>
                                    @empty
                                     <option value="" disabled>No categories available</option>
                                    @endforelse

                                </select>
                            </div>
                            <div class="form-group col-sm-12 category-modal">
                                <label for="category_order" class="control-label col-sm-4">Category Order:</label>
                                <input class="form-control col-sm-2" name="category_order" id="category_order" type="number" value="1" min="1" max="{{$maxOrder}}" required>
                            </div>
                            <div class="form-group col-sm-12 category-modal">
                                <label class="control-label col-sm-4" for="color">
                                    Category Color:
                                </label>
                                <input type='text' id="add_color" name="color" value="#111"><span
                                        class="select_color_text" required> @lang('chatter::messages.editor.select_color_category')</span>
                            </div>
                            <div class="form-group col-sm-12 category-modal">
                                <input class="btn btn-success btn-sm pull-right" id="add-update-submit" type="submit" value="Add Category">
                            </div>
                            <input type="hidden" id="category_id" name="id">
                            <input type="hidden" id="category_locked_status" name="locked">
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal modal-edit" id="suspendPosting">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Suspend Member Posting Privileges</h4>
                    </div>
                    <div class="modal-body">
                        <form id="suspendUser"
                              action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.management') }}/{{ Config::get('chatter.routes.suspend') }}"
                              class="form-inline" method="post">
                            <div class="form-group col-sm-12">
                                <label for="user_id" class="control-label col-sm-3">User Name:</label>
                                @csrf
                                <select id="suspend_user_id" name="suspend_user_id" class="form-control col-sm-9 select-with-search"  style="width: 50%;">

                                    <option value="" disabled selected>Select User</option>
                                    @forelse($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @empty
                                        <option value="0" disabled>No Users Found</option>
                                    @endforelse
                                </select>
                            </div>
                            <div class="col-sm-12 text-right">
                                <input class="btn btn-warning btn-sm pull-right" type="submit">
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <form id="restoreUser"
              action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.management') }}/{{ Config::get('chatter.routes.restore') }}"
              style="display: none;" method="post">
            @csrf
            <input id="restore_user_id" name="restore_user_id" type="hidden">
            <input type="submit">
        </form>
        <form id="lockCategory"
              action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.management') }}/{{ Config::get('chatter.routes.lockCategory') }}"
              style="display: none;" method="post">
            @csrf
            <input id="lock_category_id" name="lock_category_id" type="hidden">
            <input id="lock_category_status" name="lock_category_status" type="hidden">
            <input type="submit">
        </form>

@endsection
@section('after-scripts-end')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ url('/vendor/epmdev/chatter/assets/vendor/spectrum/spectrum.js') }}"></script>
    <script src="{{ url('/vendor/epmdev/chatter/assets/js/chatter.js') }}"></script>
    <script>
        jQuery(document).ready(function($){
            $('.select-with-search').select2({
                theme: 'bootstrap',
                width: 'resolve'
            });

            $('#add_parent').on('click', function () {
                if ($(this).is(':checked')) {
                    $('#parent_group').removeClass('hidden');
                } else {
                    $('#parent_group').addClass('hidden');
                }

            })

            $('.suspend_user').on('click', function() {
                document.getElementById('suspend_user_id').value = $(this).attr('data-user-id');
                var form = $('#suspendUser');

                if (confirm('Are you sure you wish to suspend this users posting privilages?')) {
                    form.submit();
                }
            });

            $('.restore_user').on('click', function() {
                document.getElementById('restore_user_id').value = $(this).attr('data-user-id');
                var form = $('#restoreUser');

                if (confirm('Are you sure you wish to restore this users posting privilages?')) {
                    form.submit();
                }
            });

            $('.lock-category').on('click', function () {
                var status = $(this).attr('data-category-status');
                var statusWord = '';
                document.getElementById('lock_category_id').value = $(this).attr('data-category-id');
                document.getElementById('lock_category_status').value = status;
                var form = $('#lockCategory');
                if (status == 1) { statusWord = 'unlock'; } else { statusWord = 'lock'; };
                if (confirm('Are you sure you want to ' + statusWord + ' this category?')) {
                    form.submit();
                }
            });

            $('.edit-category').on('click', function () {
                if(confirm('A you sure you wish to edit this category? This will affect all child discussions of this ' +
                    'category.')) {
                    $('#add_color').spectrum({
                        color: $(this).attr('data-category-color')
                    });
                    $('#addCategoryForm').attr('action', '/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.management') }}/{{ Config::get('chatter.routes.editCategory') }}');
                    $('#category_name').val($(this).attr('data-category-name'));
                    if ($(this).attr('data-category-parent') != 0) {
                        $('#parent_group').removeClass('hidden');
                        $('#add_parent').attr('checked', 'checked');
                        $('#parent_category').val($(this).attr('data-category-parent'));
                    }
                    $('#add_color').spectrum({
                        color:$(this).attr('data-category-color'),
                        preferredFormat: "hex",
                        containerClassName: 'chatter-color-picker',
                        cancelText: '',
                        chooseText: 'close',
                        move: function(color) {
                            $("#add_color").val(color.toHexString());
                        }
                    });
                    var category_id = parseInt($(this).attr('data-category-id'));
                    $('#parent_category option[value="' + category_id + '"]').attr('disabled', 'disabled');

                    categorySelectOptionsAvailable(category_id);



                    $('#category_id').val($(this).attr('data-category-id'));
                    $('#category_locked_status').val($(this).attr('data-category-status'));
                    $('#add-update-modal').text('Update Forum Category');
                    $('#add-update-submit').val('Update Category');
                    $('#categoryAdd').show();

                };
            });

            $('.delete-category').on('click', function () {
                var categoryName = $(this).attr('data-category-name');
                if(confirm('A you sure you wish to delete the category '+ categoryName + '? Warning! All child discussons ' +
                    'and child categories will be deleted!! This cannot be undone! Are you sure you wish to continue?')) {
                    var categoryId = $(this).attr('data-category-id');

                    var form = $('#category-delete-' + categoryId);
                    if(form.length > 0) {
                        if (confirm('Are you absolutely certain you wish to delete the category ' + categoryName +'? Any ' +
                            'posts in this category will no longer be accessable. Consider locking this category instead. ' +
                            'Are you sure you wish to continue and delete ' + categoryName + '?')) {
                            form.submit();
                        }
                    }
                }
            });

            $('.close').on('click', function() {
                $(this).trigger('hide.bs.modal');
                $(this).closest('.modal').hide();
            });

            $("#categoryAdd").on('hide.bs.modal', function() {
                var form = $("#addCategoryForm");
                // clear values and reset modal to add category type
                $("#addCategoryForm").attr('action', '/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.management') }}/{{ Config::get('chatter.routes.addCategory') }}');
                $("#addCategoryForm").get(0).reset()
                $("#parent_category option").prop('disabled', false);
                $('#add-update-modal').text('Add Forum Category');
                $('#add-update-submit').val('Add Category');
            });

            $("#add_color").spectrum({
                color: "#333639",
                preferredFormat: "hex",
                containerClassName: 'chatter-color-picker',
                cancelText: '',
                chooseText: 'close',
                move: function(color) {
                    $("#add_color").val(color.toHexString());
                }
            });
        });

        function categorySelectOptionsAvailable (id) {
            var select = document.getElementById('parent_category');
            for (var i = 0; i < select.length; i++) {
                var option = select.options[i];
                var optionParentID = parseInt(option.dataset.parentId);
                if (optionParentID === id) {
                    option.disabled = true;
                }
            }
        }
    </script>
@endsection
