<?php

namespace EpmDev\Chatter\Helpers;

use EpmDev\Chatter\Models\Category;
use EpmDev\Chatter\Models\Discussion;
use EpmDev\Chatter\Models\SuspendedUsers;

class ChatterHelper
{
    /**
     * Convert any string to a color code.
     *
     * @param string $string
     *
     * @return string
     */
    public static function stringToColorCode($string)
    {
        $code = dechex(crc32($string));

        return substr($code, 0, 6);
    }

    /**
     * User link.
     *
     * @param mixed $user
     *
     * @return string
     */
    public static function userLink($user)
    {
        $url = config('chatter.user.relative_url_to_profile', '');

        if ('' === $url) {
            return '#_';
        }

        return static::replaceUrlParameter($url, $user);
    }

    /**
     * Is User suspended?
     * @param User $user
     * @return bool
     */
    public static function userCanPost($user)
    {
        $canPost = SuspendedUsers::where('user_id', '=', $user->id)->where('suspended', '=', 1)->first();

        if (empty($canPost->user_id) || (!empty($canPost->user_id) && $canPost->suspended == 0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Is Discussion locked?
     * @param $discussion_id
     * @return bool
     */
    public static function discussionLocked($category_slug, $discussion_id)
    {
        $discussion = Discussion::find($discussion_id);
        if(!empty($discussion->id)) {
            $category = Category::where('slug', '=', $category_slug)->first();
            $categoryLocked = false;
            // If config is set to autolock discussions - return discussion locked
            if (config('chatter.category_locked') && $category->locked) {
                $categoryLocked = true;
            }
            if ($discussion->locked || $categoryLocked) {
                return true;
            }
        }
        return false;
    }

    /**
     * Replace url parameter.
     *
     * @param string $url
     * @param mixed  $source
     *
     * @return string
     */
    private static function replaceUrlParameter($url, $source)
    {
        $parameter = static::urlParameter($url);

        return str_replace('{'.$parameter.'}', $source[$parameter], $url);
    }

    /**
     * Url parameter.
     *
     * @param string $url
     *
     * @return string
     */
    private static function urlParameter($url)
    {
        $start = strpos($url, '{') + 1;

        $length = strpos($url, '}') - $start;

        return substr($url, $start, $length);
    }

    /**
     * This function will demote H1 to H2, H2 to H3, H4 to H5, etc.
     * this will help with SEO so there are not multiple H1 tags
     * on the same page.
     *
     * @param HTML string
     *
     * @return HTML string
     */
    public static function demoteHtmlHeaderTags($html)
    {
        $originalHeaderTags = [];
        $demotedHeaderTags = [];

        foreach (range(100, 1) as $index) {
            $originalHeaderTags[] = '<h'.$index.'>';

            $originalHeaderTags[] = '</h'.$index.'>';

            $demotedHeaderTags[] = '<h'.($index + 1).'>';

            $demotedHeaderTags[] = '</h'.($index + 1).'>';
        }

        return str_ireplace($originalHeaderTags, $demotedHeaderTags, $html);
    }

    /**
     * This function construct the categories menu with nested categories.
     *
     * @param array $categories
     *
     * @return string
     */
    public static function categoriesMenu($categories)
    {
        $menu = '<ul class="nav nav-pills nav-stacked">';

        foreach ($categories as $category) {
            $menu .= '<li>';
            $menu .= '<a href="/'.config('chatter.routes.home').'/'.config('chatter.routes.category').'/'.$category['slug'].'">';
            $menu .= '<div class="chatter-box" style="background-color:'.$category['color'].'"></div>';
            $menu .= $category['name'].'</a>';

            if (count($category['children'])) {
                $menu .= static::categoriesMenu($category['children']);
            }

            $menu .= '</li>';
        }

        $menu .= '</ul>';

        return $menu;
    }
}
