<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatterPostTable extends Migration
{
    public function up()
    {
        Schema::create('chatter_post', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chatter_discussion_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->binary('body');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE chatter_post MODIFY COLUMN body LONGBLOB");
    }

    public function down()
    {
        Schema::drop('chatter_post');
    }
}
